## Deliveries
In this folder are present only and exclusively the official handout

## Changelog
#### RASD
 - Version 1 - 12/16/2015 - Removed the assumption that the taxi driver must keep in the same GPS location (in the same zone) once he/she set his/her availability

#### DD
 - Version 1 - 12/15/2015 - Fix on deployment diagram
 - Version 2 - 12/16/2015 - Added the specification and the design regarding the RASD update
