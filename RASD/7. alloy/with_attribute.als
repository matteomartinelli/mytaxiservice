sig _Integer {}
sig _String {}
sig _Float {}

 	
sig Location{
	lat: one _Float,
	lon: one _Float
}

sig Zone{
	zid: _Integer,
	vertex: some Location
}

sig Queue{
	qid: _Integer,
	zone: one Zone,
	available_drivers: set TaxiDriver
}

abstract sig User{
	uid: _Integer,
	email:_String,
	password: _String,
}
sig TaxiDriver extends User{
	place: _String
}

sig Passenger extends User{

}

sig Ride{
	origin: one Location,
	destination: some Location, // More than one in shared ride
	start: one _Integer, //UNIX timestamp
	has: some Passenger, // More than one in Shared ride
	assigned_to: lone TaxiDriver,
	type: one Int
}{
	type = 0 or  	// means Request
	type = 1 or		// means Reservation
	type = 2			// means Shared
}

fact UnivocityProperties{
	/* TaxiDriver and passenger must have different ids and emails*/
	all disj p1,p2: Passenger | p1.uid != p2.uid and p1.email != p2.email
	all disj t1,t2: TaxiDriver | t1.uid != t2.uid and t1.email != t2.email
	all p: Passenger,t:TaxiDriver | p.uid != t.uid

	/* Zone with different ids */
	all disj z1,z2: Zone | z1.zid != z2.zid

	/* Queue with different ids */
	all disj q1,q2: Queue | q1.qid != q2.qid
}

fact QueueProperties{
	/* One zone is associated to one and only one queue */
	all q1, q2 : Queue |  q1!=q2  implies	q1.zone  != q2.zone
	/* One queue is associated to one and only one zone */
	all z : Zone | one q: Queue | q.zone = z
}

fact TaxiDriverProperties{
	/* One taxi driver can be at most in one queue at a given time */
	no disj q1,q2: Queue, t:TaxiDriver |  t in q1.available_drivers and t in q2.available_drivers

	/* If one taxi driver is assigned to a request, he must not belong to any queue */
	all t:TaxiDriver,r:Ride,q:Queue | r.assigned_to=t implies t not in q.available_drivers

	/* A taxiDriver can serve at most one request at once */
	no disj r1,r2:Ride | r1.assigned_to=r2.assigned_to
}

fact PassengerProperties{
	/* At most one request/reservation can be assigned at a given time */
	all p:Passenger,r1,r2:Ride | (r1 != r2 and p in r1.has and p in r2.has) implies (r1.assigned_to=none or r2.assigned_to=none)

	/* A passenger can't have two Ride of type = 0 that means 2 request*/
	all p:Passenger, r1,r2:Ride |( r1!= r2 and p in r1.has and p in r2.has and r1.type=0) implies (r2.type =1 or r2.type=2)
}

fact RideProperties{
	/* Any request has exactly one passenger */
	all r:Ride | r.type=0 implies #(r.has) = 1

	/* Any reservation has exactly one passenger */
	all r:Ride | r.type=1 implies #(r.has) = 1

	/* Number of destination must be equal to the number of passenger */
	all r:Ride | #(r.destination) = #(r.has)

	/* Every destination must have different origin and destination*/
	all r:Ride | r.origin not in r.destination
}

fact LocationProperties{
	/* Every location is assigned to a location */
	all l:Location | some r:Ride | l in r.origin or l in r.destination
}

fact ZoneProperties{
	/* Every zone has N = 4 vertex */
	all z:Zone | #(z.vertex) = 4
}

run {
} for 7
 
// ##### assert
/* There are no taxidriver which has assigned two different rides */
assert noTaxiDriverTwoRidesAssigned{
	no t:TaxiDriver | some disj r1,r2:Ride | r1.assigned_to = t and r2.assigned_to = t
}
check noTaxiDriverTwoRidesAssigned

/* There no exist taxi driver which are in two different queues at a given time  */
assert noTaxiDriverAreInTwoDifferentQueue{
	no t:TaxiDriver | some disj q1,q2:Queue | t in q1.available_drivers and t in q2.available_drivers
}
check noTaxiDriverAreInTwoDifferentQueue 

/* There no exist passenger with more than one request */
assert noPassengerWithMoreThanOneRequest{
	no disj r1,r2:Ride | r1.type=0 and r2.type=0 and r1.has = r2.has
}
check noPassengerWithMoreThanOneRequest

/* There no exist passenger with more than one ride with a taxi driver assigned at a given time*/
assert noPassengerWithMoreThanTwoRideAssigned{
	no p:Passenger | some disj r1,r2:Ride | p in r1.has and p in r2.has and r1.assigned_to != none and r2.assigned_to!=none
}
check noPassengerWithMoreThanTwoRideAssigned
