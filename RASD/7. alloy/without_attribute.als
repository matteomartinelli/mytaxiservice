	
sig Location{}

sig Zone{}

sig Queue{
	zone: one Zone,
	available_drivers: set TaxiDriver
}

abstract sig User{}

sig TaxiDriver extends User{}

sig Passenger extends User{}

sig Ride{
	origin: one Location,
	destination: some Location, // More than one in shared ride
	has: some Passenger, // More than one in Shared ride
	assigned_to: lone TaxiDriver,
	type: one Int
}{
	type = 0 or  		// means Request
	type = 1 or		// means Reservation
	type = 2			// means Shared
}

// ##### fact

fact QueueProperties{
	/* One zone is associated to one and only one queue */
	all q1, q2 : Queue |  q1!=q2  implies	q1.zone  != q2.zone
	/* One queue is associated to one and only one zone */
	all z : Zone | one q: Queue | q.zone = z
}

fact TaxiDriverProperties{
	/* One taxi driver can be at most in one queue at a given time */
	no disj q1,q2: Queue, t:TaxiDriver |  t in q1.available_drivers and t in q2.available_drivers

	/* If one taxi driver is assigned to a request, he must not belong to any queue */
	all t:TaxiDriver,r:Ride,q:Queue | r.assigned_to=t implies t not in q.available_drivers

	/* A taxiDriver can serve at most one request at once */
	no disj r1,r2:Ride | r1.assigned_to=r2.assigned_to
}

fact PassengerProperties{
	/* At most one request/reservation can be assigned at a given time */
	all p:Passenger,r1,r2:Ride | (r1 != r2 and p in r1.has and p in r2.has) implies (r1.assigned_to=none or r2.assigned_to=none)

	/* A passenger can't have two Ride of type = 0 that means 2 request*/
	all p:Passenger, r1,r2:Ride |( r1!= r2 and p in r1.has and p in r2.has and r1.type=0) implies (r2.type =1 or r2.type=2)
}

fact RideProperties{
	/* Any request has exactly one passenger */
	all r:Ride | r.type=0 implies #(r.has) = 1

	/* Any reservation has exactly one passenger */
	all r:Ride | r.type=1 implies #(r.has) = 1

	/* Number of destination must be equal to the number of passenger */
	all r:Ride | #(r.destination) = #(r.has)

	/* Every destination must have different origin and destination*/
	all r:Ride | r.origin not in r.destination
}

fact LocationProperties{
	/* Every location is assigned to a location */
	all l:Location | some r:Ride | l in r.origin or l in r.destination
}

run {
} for 7
 
// ##### assert
/* There are no taxidriver which has assigned two different rides */
assert noTaxiDriverTwoRidesAssigned{
	no t:TaxiDriver | some disj r1,r2:Ride | r1.assigned_to = t and r2.assigned_to = t
}
check noTaxiDriverTwoRidesAssigned

/* There no exist taxi driver which are in two different queues at a given time  */
assert noTaxiDriverAreInTwoDifferentQueue{
	no t:TaxiDriver | some disj q1,q2:Queue | t in q1.available_drivers and t in q2.available_drivers
}
check noTaxiDriverAreInTwoDifferentQueue 

/* There no exist passenger with more than one request */
assert noPassengerWithMoreThanOneRequest{
	no disj r1,r2:Ride | r1.type=0 and r2.type=0 and r1.has = r2.has
}
check noPassengerWithMoreThanOneRequest

/* There no exist passenger with more than one ride with a taxi driver assigned at a given time*/
assert noPassengerWithMoreThanTwoRideAssigned{
	no p:Passenger | some disj r1,r2:Ride | p in r1.has and p in r2.has and r1.assigned_to != none and r2.assigned_to!=none
}
check noPassengerWithMoreThanTwoRideAssigned
